---
title: Rammus Chromebooks
---

`rammus` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

  - [ASUS Chromebook Flip C434](https://www.asus.com/us/Commercial-Laptops/ASUS-Chromebook-Flip-C434TA/)
  - [ASUS Chromebook Flip
  C433](https://www.asus.com/Laptops/For-Home/Chromebook/ASUS-Chromebook-Flip-C433/)
  - [ASUS Chromebook C425](https://www.asus.com/Laptops/For-Students/Chromebook/ASUS-Chromebook-C425).

These chromebooks use 64 bit Intel Amber Lake Y processors like the Core
i5-8200Y, the Core m3-8100Y and the Core i7-8500Y.

### Debugging interfaces

`rammus` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the Asus C433, the debug port is the USB-C port in the right side.

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

The R8152 Ethernet driver in Depthcharge doesn't seem reliable when
working at Gigabit speeds. It's recommeded to configure the link as Fast
Ethernet when booting over TFTP.

### Example kernel command line arguments

```
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS2,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

the IP and path of the NFS share are examples and should be adapted to
the test setup.
