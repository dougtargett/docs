---
title: Bootloader setup for LAVA
weight: 4
---

As all recent Chromebooks, the boot process for coral boards is based on
[Coreboot](https://www.coreboot.org/) and
[Depthcharge](https://chromium.googlesource.com/chromiumos/platform/depthcharge).

In order to make Chromebooks usable in a LAVA lab, Depthcharge needs to
be modified to include some additional features such as TFTP support,
the ability to pass a ramdisk parameter to x86_64 kernels and enabling
the command line interface. We keep these changes for every Chromebook
board in [this repo](https://gitlab.collabora.com/chromium/depthcharge),
each
[branch](https://gitlab.collabora.com/chromium/depthcharge/-/branches)
adds the appropriate changes to a particular board starting on a
specific base version.

Chrome OS firmwares contain some binary blobs that we can't build from
scratch, so a way to generate a firmware with a modified version of
Depthcharge is to take a release firmware binary from Google and
replacing the payload with a custom modified Depthcharge binary built
from the branch above. More info
[here](https://gitlab.collabora.com/chromium/firmware-tools).


## Chromebook bootup in a LAVA setup

Chromebooks used in a LAVA environment will boot a Linux kernel through
TFTP. In order to do this, LAVA will access the Depthcharge CLI through
a serial terminal and enter the appropriate commands there. Assuming the
Chromebook has a network link to a DHCP and TFTP server, a Linux kernel
can be booted like this:

```
(depthcharge): enet dhcp
(depthcharge): tftpboot dhcp bzImage args initrd.cpio.gz
```

The first line configures the network interface in Depthcharge using
DHCP and the second one boots a Linux kernel. This assumes that the TFTP
server is serving the kernel binary (`bzImage`), the initrd
(`initrd.cpio.gz`) and a plain text file containing the kernel command
line arguments (`args`), which will be board-specific.
